# ttrss_movie_info_titi

tt-rss plugin to help get movie info from [TMDB](https://www.themoviedb.org) or [OMDB](https://www.omdbapi.com/) database from feed title.


**Installation**

Checkout the directory into your tt-rss plugins.local folder like this :

$ cd .../tt-rss/

$ git clone https://codeberg.org/Spigola/ttrss_movie_info_titi.git plugins.local/movie_info_titi

Be sure that the folder name is movie_info_titi and not ttrss_movie_info_titi

Then enable the plugin in TT-RSS preferences, set your API keys, enjoy


**Add more easily filters to all top/low rated movie (ex:to modify score)**


For "Top rated movie" search in **content** for 9737 ex : \b(9737)\b

For "Low rated movie" search in **content** for 9731 ex : \b(9731)\b
