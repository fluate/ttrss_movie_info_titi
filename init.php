<?php

class MovieInfo
{
	private $imdbLinkPrefix = "https://imdb.com/title/";
	private $imdbLinkPrefixMobile = "https://m.imdb.com/title/";

	public $original_title = '';
	public $title = '';
	public $year = '';
	public $imdbID = '';
	public $tmdbID = '';
	public $plot = '';
	public $director = '';
	public $poster = '';

	public $imdbScore = '';
	public $imdbVotes = '';
	public $imdbLink = '';
	public $imdbLinkMobile = '';

	public $tmdbVoteAverage = '';
	public $tmdbPopularity = '';
	public $rottenTomatoesScore = '';
	public $metacriticScore = '';
	public $genre = '';
	public $country = '';
	public $awards = '';

	function updateImdbLinks()
	{
		$this->imdbLink = $this->imdbLinkPrefix . $this->imdbID;
		$this->imdbLinkMobile = $this->imdbLinkPrefixMobile . $this->imdbID;
	}
	/*
	function about()
	{
		return array(
			1.0,
			"Grab movie Info from internet movie db providers",
			"Spi",
			false
		);
	}

	function init($host)
	{
	}
	*/
}

define("MOVIE_NAME_REGEX_DEFAULT", "/(.*?)([1-2][0|9][0-9][0-9])/");
define("ENABLE_TITLE_REFORMAT_DEFAULT", true);
define("ENABLE_CONTENT_REPLACE_DEFAULT", true);

class Movie_Info_Titi extends Plugin
{
	private $host;

	private $debugHookArticleFilter = false;

	private $topRatedMinScoreDefault = 7.0;
	private $lowRatedMaxScoreDefault = 5.0;

	private $optionDomainsArrayDefault = array('default');
	private $posterHDDefault = true;
	private $enableTitleReformatArrayDefault = array(ENABLE_TITLE_REFORMAT_DEFAULT);
	private $enableContentReplaceArrayDefault = array(ENABLE_CONTENT_REPLACE_DEFAULT);
	private $titlePreReplaceRegexArrayDefault = array('/\./,/3d/,/\s{2,}/');
	private $titlePreReplaceReplacementArrayDefault = array(' ,, ');
	private $movieNameRegexArrayDefault = array(MOVIE_NAME_REGEX_DEFAULT);

	private $topRatedMovieTag = "<b>Top rated movie &#9737;</b>";
	private	$lowRatedMovieTag = "<b>Low rated movie &#9731;</b>";

	function about()
	{
		return array(
			1.0,
			"Grab movie Info from internet movie db providers",
			"Spi",
			false
		);
	}

	function init($host)
	{
		$this->host = $host;

		//$host->add_hook($host::HOOK_RENDER_ARTICLE_CDM, $this);
		//$host->add_hook($host::HOOK_RENDER_ARTICLE, $this);
		$host->add_hook($host::HOOK_ARTICLE_FILTER, $this);
		$host->add_hook($host::HOOK_PREFS_TAB, $this);
		$host->add_hook($host::HOOK_PREFS_EDIT_FEED, $this);
		$host->add_hook($host::HOOK_PREFS_SAVE_FEED, $this);
	}

	function hook_prefs_tab($args)
	{
		if ($args != "prefFeeds") return;

		?>
			<div dojoType='dijit.layout.AccordionPane'
			title="<i class='material-icons'>extension</i> <?= __('Movie Info settings (Movie_Info_Titi)') ?>">
		<?php

		if (version_compare(PHP_VERSION, '7.0.0', '<'))
		{
			print_error("This plugin requires PHP 7.0.");
		}
		else
		{
			// main form
			//print_notice("Movie Info Settings");
			?>
				<br><h3><?= __("Global settings") ?></h3>
				<form dojoType='dijit.form.Form'>
				<?= \Controls\pluginhandler_tags($this, "save") ?>

				<script type="dojo/method" event="onSubmit" args="evt">
					evt.preventDefault();
					if (this.validate()) {
						Notify.progress('Saving data...', true);
						xhr.post("backend.php", this.getValues(), (reply) => {
							Notify.info(reply);
						})
					}
				</script>
			<?php

			// main vars
			$tmdbApiKey = $this->host->get($this, "tmdbApiKey");
			$omdbApiKey = $this->host->get($this, "omdbApiKey");

			$topRatedMinScore = $this->host->get($this, "topRatedMinScore");
			if ($this->IsNullOrEmptyString($topRatedMinScore)) $topRatedMinScore = $this->topRatedMinScoreDefault;
			$lowRatedMaxScore = $this->host->get($this, "lowRatedMaxScore");
			if ($this->IsNullOrEmptyString($lowRatedMaxScore)) $lowRatedMaxScore = $this->lowRatedMaxScoreDefault;
			$posterHD = $this->host->get($this, "posterHD");
			if (!isset($posterHD))	$posterHD = $this->posterHDDefault;
			$optionDomainsArray = $this->host->get($this, "optionDomainsArray");
			if (!isset($optionDomainsArray)) $optionDomainsArray = $this->optionDomainsArrayDefault;
			$enableTitleReformatArray = $this->host->get($this, "enableTitleReformatArray");
			if (!isset($enableTitleReformatArray)) $enableTitleReformatArray = $this->enableTitleReformatArrayDefault;
			$enableContentReplaceArray = $this->host->get($this, "enableContentReplaceArray");
			if (!isset($enableContentReplaceArray)) $enableContentReplaceArray = $this->enableContentReplaceArrayDefault;
			$titlePreReplaceRegexArray = $this->host->get($this, "titlePreReplaceRegexArray");
			if (!isset($titlePreReplaceRegexArray)) $titlePreReplaceRegexArray = $this->titlePreReplaceRegexArrayDefault;
			$titlePreReplaceReplacementArray = $this->host->get($this, "titlePreReplaceReplacementArray");
			if (!isset($titlePreReplaceReplacementArray)) $titlePreReplaceReplacementArray = $this->titlePreReplaceReplacementArrayDefault;
			$movieNameRegexArray = $this->host->get($this, "movieNameRegexArray");
			if (!isset($movieNameRegexArray)) $movieNameRegexArray = $this->movieNameRegexArrayDefault;

			$customDomainsCount = count($optionDomainsArray) - 1;

			print "<br>";

			// TMDB API key
			print "<label>" . __("TMDB API key :") . "</label> ";
			print "<input dojoType=\"dijit/form/TextBox\"
				data-dojo-props=\"trim:false, propercase:false\"
				id='Movie_Info_Titi_TmdbApiKey'
				name=\"tmdbApiKey\" value=\"$tmdbApiKey\">";
			print "<label>" . __("	-> <a href=\"https://www.themoviedb.org/settings/api\" target=_blank>https://www.themoviedb.org/settings/api</a> (something like this: 442ceb6ccfg7c823a7e7x3d11af7223d)") . "</label> ";

			print "<br><br>";

			// OMDB API key
			print "<label>" . __("OMDB API key :") . "</label> ";
			print "<input dojoType=\"dijit/form/TextBox\"
				data-dojo-props=\"trim:false, propercase:false\"
				id='Movie_Info_Titi_OmdbApiKey'
				name=\"omdbApiKey\" value=\"$omdbApiKey\">";
			print "<label>" . __("	-> <a href=\"https://www.omdbapi.com/apikey.aspx\" target=_blank>https://www.omdbapi.com/apikey.aspx</a> (something like this: b12d6e54)") . "</label>";

			print "<br><br>";

			// Poster height
			$this->printCheckBox("posterHD", $posterHD, "Show poster in high definition");
			print "<br><br>";
			print "<br><hr size=1><br>";

			// score settings
			print "<h3>" . __("Scoring settings") . "</h3><br>";

			// top Rated Min Score
			print "<label>" . __("Top rated min score :") . "</label> ";
			print "<input dojoType=\"dijit.form.NumberSpinner\"
				id='Movie_Info_Titi_TopRatedMinScore'
				data-dojo-props=\"constraints:{min:0,max:10}\"
				required=\"1\" name=\"topRatedMinScore\" value=\"$topRatedMinScore\">";
			print "<label>" . __("	-> feeds high score content will be tagged with '$this->topRatedMovieTag'") . "</label> ";

			print "<br><br>";

			// low Rated Max Score
			print "<label>" . __("Low rated max score :") . "</label> ";
			print "<input dojoType=\"dijit.form.NumberSpinner\"
				id='Movie_Info_Titi_LowRatedMaxScore'
				data-dojo-props=\"constraints:{min:0,max:10}\"
				required=\"1\" name=\"lowRatedMaxScore\" value=\"$lowRatedMaxScore\">";
			print "<label>" . __("	-> feeds low score content will be tagged with '$this->lowRatedMovieTag'") . "</label> ";

			print "<br><br><hr size=1><br>";
			print "<h3>Add extra regex rules for a specific domain</h3><br>";

			// nb Domains
			print "<label>" . __("Number of extra regex rules :") . "</label> ";
			print "<input dojoType=\"dijit.form.NumberSpinner\"
				id='Movie_Info_Titi_CustomDomainsCount'
				data-dojo-props=\"constraints:{min:0,max:99}\"
				required=\"1\" name=\"customDomainsCount\" value=\"$customDomainsCount\">";
			print "<br><br><a href=\"prefs.php\">After saving reload this page to see the changes</a><br><br>";
			print "<br> <h2>" . __("Domain settings") . "</h2><br>";

			for ($i = 0; $i < count($optionDomainsArray); $i++)
			{
				print "----- ";

				// domain name
				$domainName = $optionDomainsArray[$i];
				if ($domainName == 'default')
				{
					print "<label>" . __("Domain name : $domainName") . "</label> ";
				}
				else
				{
					print "<label>" . __("Domain name : ") . "</label> ";
					print "<input dojoType=\"dijit/form/TextBox\"
						data-dojo-props=\"trim:false, propercase:false\"
						id='Movie_Info_Titi_DomainName$i'
						name=\"domainName$i\" value=\"$domainName\">";
				}

				print "<br><br>";

				// enable title reformat
				$enableTitleReformat = $enableTitleReformatArray[$i];
				$this->printCheckBox("enableTitleReformat$i", $enableTitleReformat, "Reformat feed title (rename 'movie.name.year.info' into 'movie name (year) - score)'");
				print "<br>";

				// enable content replace
				$enableContentReplace = $enableContentReplaceArray[$i];
				$this->printCheckBox("enableContentReplace$i", $enableContentReplace, "Replace existing content (otherwise add below existing content");
				print "<br>";

				// title replace regex
				$titlePreReplaceRegex = $titlePreReplaceRegexArray[$i];
				print "<label>" . __("Title replace regex array :") . "</label> ";
				print "<input dojoType=\"dijit/form/TextBox\"
				data-dojo-props=\"trim:false, propercase:false\"
				id='Movie_Info_Titi_TitlePreReplaceRegex$i'
				name=\"titlePreReplaceRegex$i\" value=\"$titlePreReplaceRegex\">";

				// title replace regex replacement
				$titlePreReplaceReplacement = $titlePreReplaceReplacementArray[$i];
				print "<label>" . __("        Title replacement array :") . "</label> ";
				print "<input dojoType=\"dijit/form/TextBox\"
				data-dojo-props=\"trim:false, propercase:false\"
				id='Movie_Info_Titi_TitlePreReplaceReplacement$i'
				name=\"titlePreReplaceReplacement$i\" value=\"$titlePreReplaceReplacement\">";

				print "<label>" . __("	-> feed title must be cleaned with this regex to be exactly as the movie name. blanks instead of points. regex and replacement can be separeted by ','") . "</label> ";

				print "<br><br>";

				// movie name regex
				$movieNameRegex = $movieNameRegexArray[$i];
				print "<label>" . __("Movie name and year detection regex :") . "</label> ";
				print "<input dojoType=\"dijit/form/TextBox\"
				data-dojo-props=\"trim:false, propercase:false\"
				id='Movie_Info_Titi_MovieNameRegex$i'
				name=\"movieNameRegex$i\" value=\"$movieNameRegex\">";

				print "<br><br>";
			}

			// save buttons
			?>
				<?= \Controls\submit_tag(__("Save")) ?>
			<?php

			// end of form
			?>	
				</form><br>
			<?php

			// enabled feeds
			$enabled_feeds = $this->filter_unknown_feeds(
				$this->get_stored_array("enabled_feeds"));

			$append_feeds = $this->filter_unknown_feeds(
				$this->get_stored_array("append_feeds"));

			$this->host->set($this, "enabled_feeds", $enabled_feeds);
			$this->host->set($this, "append_feeds", $append_feeds);

			if (count($enabled_feeds) > 0) { 
			?>
				<hr/>
				<h3><?= __("Currently enabled for (click to edit):") ?></h3>

				<ul class='panel panel-scrollable list list-unstyled'>
					<?php foreach ($enabled_feeds as $f) { ?>
						<li>
							<i class='material-icons'>rss_feed</i>
							<a href='#'	onclick="CommonDialogs.editFeed(<?= $f ?>)">
									<?= Feeds::_get_title($f) . " " . (in_array($f, $append_feeds) ? __("(append)") : "") ?>
							</a>
						</li>
					<?php } ?>
				</ul>
			<?php
			}
		}

		?>
			</div>
		<?php
	}

	private function get_stored_array($name) 
	{
		$tmp = $this->host->get($this, $name);
		if (!is_array($tmp)) $tmp = [];
		return $tmp;
	}

	function printCheckBox($name, $value, $desc)
	{
		?>
		<fieldset class='narrow'>
			<label class='checkbox'>
				<?= \Controls\checkbox_tag(" " . $name, $value) ?>
				<?= __($desc) ?>
			</label>
		</fieldset>
		<?php
	}

	function save()
	{
		$tmdbApiKey = (string) $_POST["tmdbApiKey"];
		$omdbApiKey = (string) $_POST["omdbApiKey"];
		$topRatedMinScore = (float) $_POST["topRatedMinScore"];
		$lowRatedMaxScore = (float) $_POST["lowRatedMaxScore"];
		$posterHD =	isset($_POST["posterHD"]) ? checkbox_to_sql_bool($_POST["posterHD"]) : false;

		// value clamp
		if ($topRatedMinScore < 0) $topRatedMinScore = 0;
		if ($topRatedMinScore > 10) $topRatedMinScore = 10;
		if ($lowRatedMaxScore < 0) $lowRatedMaxScore = 0;
		if ($lowRatedMaxScore > 10) $lowRatedMaxScore = 10;

		$topRatedMinScore = sprintf("%.1f", $topRatedMinScore);
		$lowRatedMaxScore = sprintf("%.1f", $lowRatedMaxScore);

		if (!$this->IsNullOrEmptyString($tmdbApiKey))
			$this->host->set($this, "tmdbApiKey", $tmdbApiKey);
		if (!$this->IsNullOrEmptyString($omdbApiKey))
			$this->host->set($this, "omdbApiKey", $omdbApiKey);

		$this->host->set($this, "topRatedMinScore", $topRatedMinScore);
		$this->host->set($this, "lowRatedMaxScore", $lowRatedMaxScore);
		$this->host->set($this, "posterHD", $posterHD);

		$enableTitleReformatArray = array();
		$enableContentReplaceArray = array();
		$titlePreReplaceRegexArray = array();
		$titlePreReplaceReplacementArray = array();
		$movieNameRegexArray = array();

		$optionDomainsArray = $this->host->get($this, "optionDomainsArray");
		if (!isset($optionDomainsArray)) $optionDomainsArray = $this->optionDomainsArrayDefault;
		for ($i = 0; $i < count($optionDomainsArray); $i++)
		{
			if ($i > 0)
				$optionDomainsArray[$i] = (string) $_POST["domainName$i"];
			$enableTitleReformatArray[$i] = isset($_POST["enableTitleReformat$i"]) ? checkbox_to_sql_bool($_POST["enableTitleReformat$i"]) : false;
			$enableContentReplaceArray[$i] = isset($_POST["enableContentReplace$i"]) ? checkbox_to_sql_bool($_POST["enableContentReplace$i"]) : false;
			$titlePreReplaceRegexArray[$i] = (string) $_POST["titlePreReplaceRegex$i"];
			$titlePreReplaceReplacementArray[$i] = (string) $_POST["titlePreReplaceReplacement$i"];
			$movieNameRegexArray[$i] = (string) $_POST["movieNameRegex$i"];
		}

		$customDomainsCount = (int) $_POST["customDomainsCount"];
		$newDomainCount = $customDomainsCount - (count($optionDomainsArray) - 1);
		if ($newDomainCount > 0)
		{
			for ($i = 0; $i < $newDomainCount; $i++)
			{
				array_push($optionDomainsArray, '');
				array_push($enableTitleReformatArray, ENABLE_TITLE_REFORMAT_DEFAULT);
				array_push($enableContentReplaceArray, ENABLE_CONTENT_REPLACE_DEFAULT);
				array_push($titlePreReplaceRegexArray, '');
				array_push($titlePreReplaceReplacementArray, '');
				array_push($movieNameRegexArray, MOVIE_NAME_REGEX_DEFAULT);
			}
		}
		else
		{
			for ($i = 0; $i < abs($newDomainCount); $i++)
			{
				array_pop($optionDomainsArray);
				array_pop($enableTitleReformatArray);
				array_pop($enableContentReplaceArray);
				array_pop($titlePreReplaceRegexArray);
				array_pop($titlePreReplaceReplacementArray);
				array_pop($movieNameRegexArray);
			}
		}

		$this->host->set($this, "optionDomainsArray", $optionDomainsArray);
		$this->host->set($this, "enableTitleReformatArray", $enableTitleReformatArray);
		$this->host->set($this, "enableContentReplaceArray", $enableContentReplaceArray);
		$this->host->set($this, "titlePreReplaceRegexArray", $titlePreReplaceRegexArray);
		$this->host->set($this, "titlePreReplaceReplacementArray", $titlePreReplaceReplacementArray);
		$this->host->set($this, "movieNameRegexArray", $movieNameRegexArray);

		echo T_sprintf("Data saved !");
	}

	function hook_prefs_edit_feed($feed_id)
	{
		$enabled_feeds = $this->get_stored_array("enabled_feeds");
		
		?>
		<header><?= __("MovieInfo") ?></header>
		<section>
			<fieldset>
				<label class='checkbox'>
					<?= \Controls\checkbox_tag("titi_MovieInfo_enabled", in_array($feed_id, $enabled_feeds)) ?>
					<?= __('Récupère les infos sur le film') ?>
				</label>
			</fieldset>
		</section>
		<?php
	}

	function hook_prefs_save_feed($feed_id)
	{
		$enabled_feeds = $this->host->get($this, "enabled_feeds");
		if (!is_array($enabled_feeds)) $enabled_feeds = array();

		$enable = isset($_POST["titi_MovieInfo_enabled"]) ? checkbox_to_sql_bool($_POST["titi_MovieInfo_enabled"]) : false;
		$key = array_search($feed_id, $enabled_feeds);

		if ($enable)
		{
			if ($key === FALSE)
			{
				array_push($enabled_feeds, $feed_id);
			}
		}
		else
		{
			if ($key !== FALSE)
			{
				unset($enabled_feeds[$key]);
			}
		}

		$this->host->set($this, "enabled_feeds", $enabled_feeds);
	}

	function hook_article_filter($article)
	{
		// test feed enabled
		$enabled_feeds = $this->host->get($this, "enabled_feeds");
		if (!is_array($enabled_feeds)) return $article;

		$key = array_search($article["feed"]["id"], $enabled_feeds);
		if ($key === FALSE) return $article;

		// test API keys
		$tmdbApiKey = $this->host->get($this, "tmdbApiKey");
		$omdbApiKey = $this->host->get($this, "omdbApiKey");
		if ($this->IsNullOrEmptyString($tmdbApiKey) && $this->IsNullOrEmptyString($omdbApiKey))
		{
			$article["content"] = "Please enter at leat one TMDB or OMDB API key in movie_info_titi plugin settings. <br><br>" . $article["content"];
			return $article;
		}

		// start filtering
		$articleTitle = $article["title"];
		$movie_title = $article["title"];
		$movie_year = "";

		// debug
		//$movie_title = "Gabriels.Inferno.2020";

		$optionDomainsArray = $this->host->get($this, "optionDomainsArray");
		if (!isset($optionDomainsArray)) $optionDomainsArray = $this->optionDomainsArrayDefault;
		$enableTitleReformatArray = $this->host->get($this, "enableTitleReformatArray");
		if (!isset($enableTitleReformatArray)) $enableTitleReformatArray = $this->enableTitleReformatArrayDefault;
		$enableContentReplaceArray = $this->host->get($this, "enableContentReplaceArray");
		if (!isset($enableContentReplaceArray)) $enableContentReplaceArray = $this->enableContentReplaceArrayDefault;
		$titlePreReplaceRegexArray = $this->host->get($this, "titlePreReplaceRegexArray");
		if (!isset($titlePreReplaceRegexArray)) $titlePreReplaceRegexArray = $this->titlePreReplaceRegexArrayDefault;
		$titlePreReplaceReplacementArray = $this->host->get($this, "titlePreReplaceReplacementArray");
		if (!isset($titlePreReplaceReplacementArray)) $titlePreReplaceReplacementArray = $this->titlePreReplaceReplacementArrayDefault;
		$movieNameRegexArray = $this->host->get($this, "movieNameRegexArray");
		if (!isset($movieNameRegexArray)) $movieNameRegexArray = $this->movieNameRegexArrayDefault;

		$articleLink = strtolower($article["link"]);
		$currentDomain = 0;

		if (count($optionDomainsArray) > 1)
		{
			for ($i = 1; $i < count($optionDomainsArray); $i++)
			{
				$find = strtolower($optionDomainsArray[$i]);
				$pos = strpos($articleLink, $find);
				if ($pos !== false)
				{
					$currentDomain = $i;
					break;
				}
			}
		}

		// pre filter
		$titlePreReplaceRegex = $titlePreReplaceRegexArray[$currentDomain];
		$titlePreReplaceReplacement = $titlePreReplaceReplacementArray[$currentDomain];

		if (!$this->IsNullOrEmptyString($titlePreReplaceRegex))
		{
			//$titlePreReplaceRegexExploded = explode(",", $titlePreReplaceRegex);
			$titlePreReplaceRegexExploded = preg_split("/(?!\/),(?=\/)/", $titlePreReplaceRegex);
			$titlePreReplaceReplacementExploded = explode(",", $titlePreReplaceReplacement);
			$movie_title = preg_replace($titlePreReplaceRegexExploded, $titlePreReplaceReplacementExploded, $movie_title);
		}

		// movie title regex
		$movieNameRegex = $movieNameRegexArray[$currentDomain];
		if (!$this->IsNullOrEmptyString($movieNameRegex) && preg_match($movieNameRegex, $movie_title, $matches))
		{
			$movie_title = $matches[1];
			$movie_year = $matches[2];
		}

		//$movie_title = str_replace(".", " ", $movie_title);
		$movie_title = trim($movie_title);

		// TMDB
		$movieInfoTMDB = $this->grabInfoFromTMDB($movie_title, $movie_year);
		$movieInfoOMDB = $this->grabInfoFromOMDB($movie_title, $movie_year);

		if ($this->debugHookArticleFilter)
		{
			print("\n######## DEBUG  ########\n");

			print("movie title : $movie_title\n");
			print("article link : $articleLink\n");
			print("domain found : $optionDomainsArray[$currentDomain]\n");

			print("titlePreReplaceRegex : $titlePreReplaceRegex\n");
			print("titlePreReplaceReplacement : $titlePreReplaceReplacement\n");
			print("movieNameRegex : $movieNameRegex\n");

			print("original title : $movieInfoTMDB->original_title\n");
			print("title : $movieInfoTMDB->title\n");
			print("imdbID : $movieInfoTMDB->imdbID\n");
			print("tmdbID : $movieInfoTMDB->tmdbID\n");
			print("plot : $movieInfoTMDB->plot\n");
			print("director : $movieInfoTMDB->director\n");
			print("poster : $movieInfoTMDB->poster\n");
			print("imdbScore : $movieInfoTMDB->imdbScore\n");
			print("imdbVotes : $movieInfoTMDB->imdbVotes\n");
			print("imdbLink : $movieInfoTMDB->imdbLink\n");
			print("imdbLinkMobile : $movieInfoTMDB->imdbLinkMobile\n");
			print("tmdbPopularity : $movieInfoTMDB->tmdbPopularity\n");
			print("tmdbVoteAverage : $movieInfoTMDB->tmdbVoteAverage\n");
			print("rottenTomatoesScore : $movieInfoTMDB->rottenTomatoesScore\n");
			print("metacriticScore : $movieInfoTMDB->metacriticScore\n");
			print("genre : $movieInfoTMDB->genre\n");
			print("country : $movieInfoTMDB->country\n");
			print("awards : $movieInfoTMDB->awards\n");

			print("\n");

			print("\n######## DEBUG OMDB ########\n");
			print("original_title : $movieInfoOMDB->original_title\n");
			print("title : $movieInfoOMDB->title\n");
			print("imdbID : $movieInfoOMDB->imdbID\n");
			print("tmdbID : $movieInfoOMDB->tmdbID\n");
			print("plot : $movieInfoOMDB->plot\n");
			print("director : $movieInfoOMDB->director\n");
			print("poster : $movieInfoOMDB->poster\n");
			print("imdbScore : $movieInfoOMDB->imdbScore\n");
			print("imdbVotes : $movieInfoOMDB->imdbVotes\n");
			print("imdbLink : $movieInfoOMDB->imdbLink\n");
			print("imdbLinkMobile : $movieInfoOMDB->imdbLinkMobile\n");
			print("tmdbPopularity : $movieInfoOMDB->tmdbPopularity\n");
			print("tmdbVoteAverage : $movieInfoOMDB->tmdbVoteAverage\n");
			print("rottenTomatoesScore : $movieInfoOMDB->rottenTomatoesScore\n");
			print("metacriticScore : $movieInfoOMDB->metacriticScore\n");
			print("genre : $movieInfoOMDB->genre\n");
			print("country : $movieInfoOMDB->country\n");
			print("awards : $movieInfoOMDB->awards\n");
			print("#######################\n\n");
		}

		// test movieInfo base info
		if ($this->IsNullOrEmptyString($movieInfoTMDB->title) && $this->IsNullOrEmptyString($movieInfoOMDB->title))
		{
			$article["content"] = "Unfortunately, no movie was found on TMDB and OMDB with the current filtered title :<br><br><b>" . $movie_title . "<b><br><br><hr size=1><br>" . $article["content"];
			return $article;
		}

		// main vars
		$topRatedMinScore = (int) $this->host->get($this, "topRatedMinScore");
		$lowRatedMaxScore = $this->host->get($this, "lowRatedMaxScore");
		if ($this->IsNullOrEmptyString($topRatedMinScore)) $topRatedMinScore = $this->topRatedMinScoreDefault;
		if ($this->IsNullOrEmptyString($lowRatedMaxScore)) $lowRatedMaxScore = $this->lowRatedMaxScoreDefault;

		$enableTitleReformat = $enableTitleReformatArray[$currentDomain];
		$enableContentReplace = $enableContentReplaceArray[$currentDomain];

		if ($this->debugHookArticleFilter)
		{
			print("\n######## DEBUG ########\n");
			print("topRatedMinScore : $topRatedMinScore\n");
			print("lowRatedMaxScore : $lowRatedMaxScore\n");
			print("enableTitleReformat : $enableTitleReformat\n");
			print("enableContentReplace : $enableContentReplace\n");
			print("#######################\n\n");
		}

		// article rewrite
		if ($enableContentReplace)
			$article["content"] = "";
		else
			$article["content"] .= "<br><br>";

		if ($enableTitleReformat)
		{
			$article["title"] = $movie_title;
			if (!$this->IsNullOrEmptyString($movie_year))
				$article["title"] .= " (" . $movie_year . ")";
			$article["content"] .= $articleTitle . "<br><br>";
		}

		// movie origin
		$amznMarker   = '.amzn.';
		$netflixMarker   = '.nf.';
		$disneyMarker   = 'disney.';

		$movieOrigin = '';
		$articleTitleLower = strtolower($articleTitle);
		if (!stripos($articleTitleLower, $amznMarker) === false)
			$movieOrigin = 'Amazon';
		else if (!stripos($articleTitleLower, $netflixMarker) === false)
			$movieOrigin = "Netflix";
		else if (!stripos($articleTitleLower, $disneyMarker) === false)
			$movieOrigin = "Disney";

		if (!$this->IsNullOrEmptyString($movieOrigin))
			$article["title"] = "[" . $movieOrigin . "] " . $article["title"];

		// IMDB score
		$imdbScore = '';
		if (!$this->IsNullOrEmptyString($movieInfoTMDB->imdbScore))
			$imdbScore = $movieInfoTMDB->imdbScore;
		else if (!$this->IsNullOrEmptyString($movieInfoOMDB->imdbScore))
			$imdbScore = $movieInfoOMDB->imdbScore;
		if (!$this->IsNullOrEmptyString($imdbScore))
			$article["content"] .= "IMDB score : " . $imdbScore;

		// IMDB votes
		$imdbVotes = '';
		if (!$this->IsNullOrEmptyString($movieInfoTMDB->imdbVotes))
			$imdbVotes = $movieInfoTMDB->imdbVotes;
		else if (!$this->IsNullOrEmptyString($movieInfoOMDB->imdbVotes))
			$imdbVotes = $movieInfoOMDB->imdbVotes;
		if (!$this->IsNullOrEmptyString($imdbVotes))
			$article["content"] .= " (" . $imdbVotes . ")";

		if (!$this->IsNullOrEmptyString($imdbScore))
			$article["content"] .= 	"<br>";

		// rating count
		$meanRating = 0;
		$meanRatingCount = 0;

		$imdbVotes = str_replace(',', '', $movieInfoTMDB->imdbVotes);
		if (!$this->IsNullOrEmptyString($imdbScore))
		{
			$meanRating += (float) $imdbScore;
			$meanRatingCount++;
		}

		// rottenTomatoesScore & metacriticScore count
		if (!$this->IsNullOrEmptyString($movieInfoOMDB->rottenTomatoesScore))
		{
			$article["content"] .= "RottenTomatoes score : " . $movieInfoOMDB->rottenTomatoesScore . "<br>";
			$meanRating += (float) $movieInfoOMDB->rottenTomatoesScore / 10.0;
			$meanRatingCount++;
		}

		if (!$this->IsNullOrEmptyString($movieInfoOMDB->metacriticScore))
		{
			$article["content"] .= "Metacritic score : " . $movieInfoOMDB->metacriticScore . "<br>";
			$meanRating += (float) $movieInfoOMDB->metacriticScore / 10.0;
			$meanRatingCount++;
		}

		if (!$this->IsNullOrEmptyString($movieInfoTMDB->tmdbVoteAverage))
		{
			$article["content"] .= "TMDB vote average : " . $movieInfoTMDB->tmdbVoteAverage . "<br>";
			$meanRating += (float) $movieInfoTMDB->tmdbVoteAverage;
			$meanRatingCount++;
		}

		// TMDB popularity
		if (!$this->IsNullOrEmptyString($movieInfoTMDB->tmdbPopularity))
		{
			$article["content"] .= "TMDB popularity : " . $movieInfoTMDB->tmdbPopularity . "<br>";
			//$meanRating += (float) $movieInfoTMDB->tmdbPopularity / 10.0;
			//$meanRatingCount++;
		}

		// total score
		if ($meanRatingCount > 0)
		{
			$meanRating = round($meanRating / $meanRatingCount, 1);
			$article["title"] .= " - " . $meanRating;

			// rating tag
			if ($meanRating >= $topRatedMinScore)
			{
				$article["content"] .= "<span><br>";
				$article["content"] .= "<img src='" . get_self_url_prefix() . "/plugins.local/movie_info_titi/top.svg'><br>";
				$article["content"] .= $this->topRatedMovieTag;
				$article["content"] .= "<br>";
			}
			else if ($meanRating <= $lowRatedMaxScore)
			{
				$article["content"] .= "<span><br>";
				$article["content"] .= "<img src='" . get_self_url_prefix() . "/plugins.local/movie_info_titi/low.svg'><br>";
				$article["content"] .= $this->lowRatedMovieTag;
				$article["content"] .= "<br>";
			}
		}

		$article["content"] .= "<br>";

		// plot
		$plot = '';
		if (!$this->IsNullOrEmptyString($movieInfoTMDB->plot))
			$plot = $movieInfoTMDB->plot;
		else if (!$this->IsNullOrEmptyString($movieInfoOMDB->plot))
			$plot = $movieInfoOMDB->plot;
		if (!$this->IsNullOrEmptyString($plot))
			$article["content"] .= "Plot : " . $plot . "<br><br>";

		// Movie title
		$movieTitle = '';
		if (!$this->IsNullOrEmptyString($movieInfoTMDB->title))
			$movieTitle = $movieInfoTMDB->title;
		else if (!$this->IsNullOrEmptyString($movieInfoOMDB->title))
			$movieTitle = $movieInfoOMDB->title;
		if (!$this->IsNullOrEmptyString($movieTitle))
			$article["content"] .= "<b>" . $movieTitle . "</b>";

		if (!$this->IsNullOrEmptyString($movieInfoTMDB->original_title) && $movieInfoTMDB->original_title != $movieInfoTMDB->title)
			$article["content"] .= " (" . $movieInfoTMDB->original_title . ")";

		$article["content"] .= "<br>";

		// movie origin
		if (!$this->IsNullOrEmptyString($movieOrigin))
			$article["content"] .= "Origin : " . $movieOrigin . "<br>";

		// genre
		$movieGenre = '';
		if (!$this->IsNullOrEmptyString($movieInfoTMDB->genre))
			$movieGenre = $movieInfoTMDB->genre;
		else if (!$this->IsNullOrEmptyString($movieInfoOMDB->genre))
			$movieGenre = $movieInfoOMDB->genre;
		if (!$this->IsNullOrEmptyString($movieGenre))
			$article["content"] .= "Genre : " . $movieGenre . "<br>";

		// director
		if (!$this->IsNullOrEmptyString($movieInfoOMDB->director))
			$article["content"] .= "Director : " . $movieInfoOMDB->director . "<br>";

		// country
		if (!$this->IsNullOrEmptyString($movieInfoOMDB->country))
			$article["content"] .= "Country : " . $movieInfoOMDB->country . "<br>";

		// awards
		if (!$this->IsNullOrEmptyString($movieInfoOMDB->awards))
			$article["content"] .= "Awards : " . $movieInfoOMDB->awards . "<br>";

		// IMDB link
		$imdbIdLink = '';
		if (!$this->IsNullOrEmptyString($movieInfoTMDB->imdbLink))
			$imdbIdLink = $movieInfoTMDB->imdbLink;
		else if (!$this->IsNullOrEmptyString($movieInfoOMDB->imdbLink))
			$imdbIdLink = $movieInfoOMDB->imdbLink;
		if (!$this->IsNullOrEmptyString($imdbIdLink))
			$article["content"] .=  "<a target=_blank href=\"" . $imdbIdLink . "\">IMDB Link</a>";

		// Search Link
		if (!$this->IsNullOrEmptyString($movieInfoTMDB->title))
			$movieTitle = $movieInfoTMDB->title;
		$article["content"] .=  " | <a target=_blank href=\"https://www.rottentomatoes.com/search?search=" . $movieTitle . "\">Rotten tomatoes</a> | ";
		$article["content"] .=  "<a target=_blank href=\"https://mubi.com/search/films?query=" . $movieTitle . "\">Mubi</a> | ";
		$article["content"] .=  "<a target=_blank href=\"https://www.youtube.com/results?search_query=" . $movieTitle . "\">Youtube</a> | ";
		$article["content"] .=  "<a target=_blank href=\"https://en.wikipedia.org/w/index.php?search=" . $movieTitle . "\">Wikipedia</a><br>";

		// poster
		$moviePosterUrl = '';
		if (!$this->IsNullOrEmptyString($movieInfoTMDB->poster))
			$moviePosterUrl = $movieInfoTMDB->poster;
		else if (!$this->IsNullOrEmptyString($movieInfoOMDB->poster))
			$moviePosterUrl = $movieInfoOMDB->poster;

		if (!$this->IsNullOrEmptyString($moviePosterUrl))
			$article["content"] .= "<br><img src=\"" . $moviePosterUrl . "\"><br>";

		return $article;
	}

	private function grabInfoFromOMDB($movie_title, $movie_year)
	{
		$movieInfo = new MovieInfo;

		// OMDB vars
		$omdbApiKey = $this->host->get($this, "omdbApiKey");
		if ($this->IsNullOrEmptyString($omdbApiKey))
			return $movieInfo;

		// OMDB url build
		$movie_title = str_replace(" ", "+", $movie_title);
		$omdbUrl = "https://www.omdbapi.com/?t=" . $movie_title;
		if (!$this->IsNullOrEmptyString($movie_year))
			$omdbUrl .= "&y=" . $movie_year;
		$omdbUrl .= "&plot=full&apikey=" . $omdbApiKey;

		$json = fetch_file_contents($omdbUrl);
		$movieJson = json_decode($json);

		if (!isset($movieJson->Title))
			return $movieInfo;

		if ($this->IsNullOrEmptyString($movieJson->Title))
			return $movieInfo;

		$movieInfo->title = $movieJson->Title;
		$movieInfo->year = $movieJson->Year;
		$movieInfo->imdbID = $movieJson->imdbID;
		$movieInfo->updateImdbLinks();
		$movieInfo->plot = $movieJson->Plot != "N/A" ? $movieJson->Plot : "";
		$movieInfo->director = $movieJson->Director != "N/A" ? $movieJson->Director : "";
		$movieInfo->poster = $movieJson->Poster != "N/A" ? $movieJson->Poster : "";
		$movieInfo->imdbScore = $movieJson->imdbRating;
		$movieInfo->imdbScore = str_replace(",", ".", $movieInfo->imdbScore);

		if (is_numeric($movieInfo->imdbScore))
		{
			$movieInfo->imdbVotes = $movieJson->imdbVotes;
		}
		else
		{
			$movieInfo->imdbScore = "";
		}

		$movieRatings = $movieJson->Ratings;
		foreach ($movieRatings as $rating)
		{
			$ratingSource = $rating->Source;
			$ratingValue = $rating->Value;
			$ratingValue = str_replace(["%", "/100", "/10"], '', $ratingValue);

			if ($ratingSource == "Rotten Tomatoes")
			{
				$movieInfo->rottenTomatoesScore = $ratingValue;
			}
			else if ($ratingSource == "Metacritic")
			{
				$movieInfo->metacriticScore = $ratingValue;
			}
		}

		$movieInfo->genre = $movieJson->Genre != "N/A" ? $movieJson->Genre : "";
		$movieInfo->country = $movieJson->Country != "N/A" ? $movieJson->Country : "";
		$movieInfo->awards = $movieJson->Awards != "N/A" ? $movieJson->Awards : "";

		return $movieInfo;
	}

	private function grabInfoFromTMDB($movie_title, $movie_year)
	{
		$movieInfo = new MovieInfo;

		$tmdbApiKey = $this->host->get($this, "tmdbApiKey");
		if ($this->IsNullOrEmptyString($tmdbApiKey))
			return $movieInfo;

		// tmdb vars
		$tmdbUrlPrefixSearchMovie = "https://api.themoviedb.org/3/search/movie?api_key="; //fba8d01f42873d7a861f712733f5ddbb&query=";
		$tmdbUrlPrefixSearchByTmdbId = "https://api.themoviedb.org/3/movie/";
		$posterPrefixOrigin = "https://image.tmdb.org/t/p/original";
		$posterPrefixHD = "https://image.tmdb.org/t/p/w780";
		$posterPrefixLD = "https://image.tmdb.org/t/p/w342";

		// infos from title
		$movie_title = str_replace(" ", "+", $movie_title);
		$tmdbUrl = $tmdbUrlPrefixSearchMovie . $tmdbApiKey . "&query=" . $movie_title;
		if (!$this->IsNullOrEmptyString($movie_year))
			$tmdbUrl .= "&year=" . $movie_year;

		$json = fetch_file_contents($tmdbUrl);
		$movieJson = json_decode($json);

		if (!isset($movieJson->results) || !is_array($movieJson->results) || count($movieJson->results) <= 0)
			return $movieInfo;

		$tmdbResults = $movieJson->results;
		$tmdbResult = $tmdbResults[0];
		$movieInfo->original_title = $tmdbResult->original_title;
		$movieInfo->title = $tmdbResult->title;
		$movieInfo->tmdbID = $tmdbResult->id;
		$movieInfo->plot = $tmdbResult->overview;
		$movieInfo->tmdbPopularity = (int) $tmdbResult->popularity;
		$movieInfo->tmdbVoteAverage = $tmdbResult->vote_average;

		if (!$this->IsNullOrEmptyString($tmdbResult->poster_path))
		{
			$posterHD = $this->host->get($this, "posterHD");
			if (isset($posterHD))
				$posterHD = $this->posterHDDefault;

			if ($posterHD)
				$movieInfo->poster = $posterPrefixHD . $tmdbResult->poster_path;
			else
				$movieInfo->poster = $posterPrefixLD . $tmdbResult->poster_path;
		}

		// advanced infos from tmdb id
		$tmdbUrl = $tmdbUrlPrefixSearchByTmdbId . $movieInfo->tmdbID . "?api_key=" . $tmdbApiKey;
		$json = fetch_file_contents($tmdbUrl);
		$movieJson = json_decode($json);

		if (!$this->IsNullOrEmptyString($movieJson->imdb_id))
		{
			$movieInfo->imdbID = $movieJson->imdb_id;
			$movieInfo->updateImdbLinks();

			$dom = new DOMDocument();
			$html = fetch_file_contents($movieInfo->imdbLinkMobile);
			$html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');

			if (empty($html))
				return $movieInfo;

			@$dom->loadHTML($html);
			$ratingDiv = $dom->getElementById('ratings-bar');

			if ($ratingDiv != NULL)
			{
				$spans = $ratingDiv->getElementsByTagName('span');
				foreach ($spans as $span)
				{
					$pattern = '/(^.*[0-9])\/([0-9]+)/';

					//print("span : $span->nodeValue\n");

					if (preg_match($pattern, $span->nodeValue, $matches))
					{
						$movieInfo->imdbScore = $matches[1];
						$movieInfo->imdbScore = str_replace(",", ".", $movieInfo->imdbScore);
						if (is_numeric($movieInfo->imdbScore))
						{
							$movieInfo->imdbVotes = $matches[2];
						}
						else
						{
							$movieInfo->imdbScore = "";
						}

						break;
					}
				}
			}
		}

		return $movieInfo;
	}

	function api_version()
	{
		return 2;
	}

	private function filter_unknown_feeds($enabled_feeds)
	{
		$tmp = array();

		foreach ($enabled_feeds as $feed)
		{

			$sth = $this->pdo->prepare("SELECT id FROM ttrss_feeds WHERE id = ? AND owner_uid = ?");
			$sth->execute([$feed, $_SESSION['uid']]);

			if ($row = $sth->fetch())
			{
				array_push($tmp, $feed);
			}
		}

		return $tmp;
	}

	private function IsNullOrEmptyString($str)
	{
		return (!isset($str) || trim($str) === '');
	}

	/*
	function hook_render_article_cdm($article) {
		return $this->hook_render_article($article);
	}
	*/
}
